from fixtures.Locators import Locators as loc


class CalcHelper:
    def __init__(self, app):
        self.app = app

    def enter_number(self, number):
        driver = self.app.driver
        for numb in list(number):
            element = loc.CalculatorPage.Help_choiche_button(loc.CalculatorPage, driver, numb).get_element()
            element.click()

    def get_result(self):
        driver = self.app.driver
        return loc.CalculatorPage.Editbox_result(driver).text()

    def get_formula(self):
        driver = self.app.driver
        return loc.CalculatorPage.Editbox_formula(driver).text()

    def string_to_float(self, result):
        return int(float(''.join(list(map(lambda x: '.' if x == ',' else x, result))))*100)/100
