class Locator:
    def __init__(self, driver, path):
        self.path = path
        self.driver = driver

    def get_element(self):
        return self.driver.find_element_by_android_uiautomator(self.path)

    def text(self):
        return self.get_element().text

    def get_attribute(self,attribute):
        return self.get_element().get_attribute(attribute)


class Locators:
    class CalculatorPage:
        def Button_0(driver):
            return Locator(driver, 'new UiSelector().resourceId("com.android.calculator2:id/digit_0")')

        def Button_1(driver):
            return Locator(driver, 'new UiSelector().resourceId("com.android.calculator2:id/digit_1")')

        def Button_2(driver):
            return Locator(driver, 'new UiSelector().resourceId("com.android.calculator2:id/digit_2")')

        def Button_3(driver):
            return Locator(driver, 'new UiSelector().resourceId("com.android.calculator2:id/digit_3")')

        def Button_4(driver):
            return Locator(driver, 'new UiSelector().resourceId("com.android.calculator2:id/digit_4")')

        def Button_5(driver):
            return Locator(driver, 'new UiSelector().resourceId("com.android.calculator2:id/digit_5")')

        def Button_6(driver):
            return Locator(driver, 'new UiSelector().resourceId("com.android.calculator2:id/digit_6")')

        def Button_7(driver):
            return Locator(driver, 'new UiSelector().resourceId("com.android.calculator2:id/digit_7")')

        def Button_8(driver):
            return Locator(driver, 'new UiSelector().resourceId("com.android.calculator2:id/digit_8")')

        def Button_9(driver):
            return Locator(driver, 'new UiSelector().resourceId("com.android.calculator2:id/digit_9")')

        def Button_point(driver):
            return Locator(driver, 'new UiSelector().resourceId("com.android.calculator2:id/dec_point")')

        def Button_equally(driver):
            return Locator(driver, 'new UiSelector().resourceId("com.android.calculator2:id/eq")')

        def Button_division(driver):
            return Locator(driver, 'new UiSelector().resourceId("com.android.calculator2:id/op_div")')

        def Button_multiplication(driver):
            return Locator(driver, 'new UiSelector().resourceId("com.android.calculator2:id/op_mul")')

        def Button_minus(driver):
            return Locator(driver, 'new UiSelector().resourceId("com.android.calculator2:id/op_sub")')

        def Button_plus(driver):
            return Locator(driver, 'new UiSelector().resourceId("com.android.calculator2:id/op_add")')

        def Button_delete(driver):
            return Locator(driver, 'new UiSelector().resourceId("com.android.calculator2:id/del")')

        def Editbox_result(driver):
            return Locator(driver, 'new UiSelector().resourceId("com.android.calculator2:id/result")')

        def Editbox_formula(driver):
            return Locator(driver, 'new UiSelector().resourceId("com.android.calculator2:id/formula")')

        def Help_choiche_button(self, driver, button_name):
            helper = {
                '0': self.Button_0(driver),
                '1': self.Button_1(driver),
                '2': self.Button_2(driver),
                '3': self.Button_3(driver),
                '4': self.Button_4(driver),
                '5': self.Button_5(driver),
                '6': self.Button_6(driver),
                '7': self.Button_7(driver),
                '8': self.Button_8(driver),
                '9': self.Button_9(driver),
                ',': self.Button_point(driver),
                '=': self.Button_equally(driver),
                'd': self.Button_delete(driver),
                '/': self.Button_division(driver),
                '*': self.Button_multiplication(driver),
                '-': self.Button_minus(driver),
                '+': self.Button_plus(driver),
            }
            return helper.get(button_name, lambda: "nothing")
