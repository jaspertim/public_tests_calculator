from appium import webdriver

from fixtures.calculator import CalcHelper


class Application:

    def CreateDriver(self,appname):
        if appname=='Youmagic':
            desired_caps = {}
            desired_caps['platformName'] = 'Android'
            desired_caps['platformVersion'] = '5.1'
            desired_caps['deviceName'] = '0123456789ABCDEF'
            desired_caps['appPackage'] = 'ru.mtt.youmagic'
            desired_caps['appActivity'] = 'ru.mtt.business.MttphoneActivity'
        elif appname=='calc':
            desired_caps = {}
            desired_caps['platformName'] = 'Android'
            desired_caps['platformVersion'] = '5.1'
            desired_caps['deviceName'] = '0123456789ABCDEF'
            desired_caps['appPackage'] = 'com.android.calculator2'
            desired_caps['appActivity'] = 'com.android.calculator2.Calculator'
        else:
            desired_caps = {}
            desired_caps['platformName'] = 'Android'
            desired_caps['platformVersion'] = '5.1'
            desired_caps['deviceName'] = '0123456789ABCDEF'
            desired_caps['appPackage'] = 'com.android.calculator2'
            desired_caps['appActivity'] = 'com.android.calculator2.Calculator'

        driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)
        driver.implicitly_wait(20)
        return driver




    def __init__(self):
        self.driver=self.CreateDriver('calc')
        self.calc=CalcHelper(self)

    def destroy(self):
        self.driver.quit()

    def find_element(self, locator):
        try:
            return locator.get_element()
        except:
            return None
