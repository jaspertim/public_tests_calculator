import allure
import pytest
import time
from allure.constants import AttachmentType

def test_1_Сложить_105_и_5(app):
    driver = app.driver
    app.calc.enter_number('105+5=')
    time.sleep(1)
    allure.attach('Скриншот экрана', driver.get_screenshot_as_png(), type=AttachmentType.PNG)
    assert float(105 + 5) == float(app.calc.get_formula()), 'Правильный ответ: 105+5=110'


def test_2_Вычесть_5412_из_числа_9999(app):
    driver = app.driver
    app.calc.enter_number('9999-5412=')
    time.sleep(1)
    allure.attach('Скриншот экрана', driver.get_screenshot_as_png(), type=AttachmentType.PNG)
    assert float(9999 - 5412) == float(app.calc.get_formula()), 'Правильный ответ: 9999-5412=4587'


def test_3_Умножить_123_на_321(app):
    driver = app.driver
    app.calc.enter_number('123*321=')
    time.sleep(1)
    allure.attach('Скриншот экрана', driver.get_screenshot_as_png(), type=AttachmentType.PNG)
    assert float(123 * 321) == float(app.calc.get_formula()), 'Правильный ответ: 123*321=39483'


def test_4_302_делить_на_15(app):
    driver = app.driver
    app.calc.enter_number('302/15=')
    result = app.calc.string_to_float(app.calc.get_formula())
    time.sleep(1)
    allure.attach('Скриншот экрана', driver.get_screenshot_as_png(), type=AttachmentType.PNG)
    assert int(float(302/15)*100)/100 == result, 'Правильный ответ: 302/15=20.13'


def test_5_15_умножить_на_10_поделить_на_3_и_сложить_15_и_вычесть_10(app):
    driver = app.driver
    with pytest.allure.step('15*10='):
        app.calc.enter_number('15*10')
        result = app.calc.string_to_float(app.calc.get_result())
        allure.attach('Скриншот экрана', driver.get_screenshot_as_png(), type=AttachmentType.PNG)
        assert int(float(15 * 10) * 100) / 100 == result, 'Правильный ответ: 15*10=150'
    with pytest.allure.step('(15*10)/3='):
        app.calc.enter_number('/3')
        result = app.calc.string_to_float(app.calc.get_result())
        allure.attach('Скриншот экрана', driver.get_screenshot_as_png(), type=AttachmentType.PNG)
        assert int(float(15 * 10/3) * 100) / 100 == result, 'Правильный ответ: (15*10)/3=50'
    with pytest.allure.step('(15*10)/3+15='):
        app.calc.enter_number('+15')
        result = app.calc.string_to_float(app.calc.get_result())
        allure.attach('Скриншот экрана', driver.get_screenshot_as_png(), type=AttachmentType.PNG)
        assert int(float(15 * 10 / 3 + 15) * 100) / 100 == result, 'Правильный ответ: (15*10)/3+15=65'
    with pytest.allure.step('(15*10)/3+15-10='):
        app.calc.enter_number('-10=')
        result = app.calc.string_to_float(app.calc.get_formula())
        time.sleep(1)
        allure.attach('Скриншот экрана', driver.get_screenshot_as_png(), type=AttachmentType.PNG)
        assert int(float(15 * 10 / 3 + 15 - 10) * 100) / 100 == result, 'Правильный ответ: (15*10)/3+15-10=55'

